# TESTE PHP MOBILE SAÚDE

O objetivo desse teste é testar o candidato nas stacks exigidas para a vaga de PHP.

## O que será analisado?

- Análise e compreensão do escopo
- Lógica e domínio sobre a liguagem PHP
- Lógica e domínio sobre o FrameWork CodeIgniter
- Organização do projeto e do código
- Compreensão de modelos de dados e modelagem de novas estruturas
- Conhecimento de Redis e ElasticSearch
- Desenvoltura com arquitetura baseada em microserviços
- Desenvoltura para lidar com sistemas em produção e criação de novos serviços
- Performance e velocidade de entrega
- Qualidade da entrega

## Resumo do escopo
- Fazer um Fork do projeto base do teste (https://bitbucket.org/mobilesaude2/teste-php/src)
- Corrigir um bug proposital existente em um dos EndPoints da API do Projeto Base
- Modelar um conjunto de novas tabelas no banco para cadastro de usuários e planos de saúde
- Criar as APIs REST com um crud para alimentar as tabelas criadas
- Implementar endpoints para registrar e consultar chamados no ElasticSearch
- Implementar uma tela de listagem de chamados com opção de busca e paginação
- Implementar um modal para adição de novos chamados, e acoplar a um botão de adicionar novo chamado no painel
- Adicionar ao projeto um dump do banco de dados que foi modelado
- Entregar uma Collection com todos os EndPoints em perfeito funcionamento
- Abrir um PULL Request do projeto editado por você

## Parte 1 - Configurar acessos do projeto
- Você receberá no seu email informações para configurar a conexão com o ElasticSearch;

## Parte 2 - Corrigir bug na API
- O projeto possui uma API que está com um erro proposital, encontre e corrija o erro na API;

## Parte 3 - Modelar o banco MySQL 8
- Criar uma tabela de planos com os campos: id e descricao;
- Criar uma tabela de pacientes com os campos: id, cpf, nome, data_nascimento, sexo, telefone, id_plano;

## Parte 4 - Contruir API
- Adicionar ao projeto uma API REST com CRUD completo para a tabela de planos, tendo como rota base `/api/plano`;
- Adicionar ao projeto uma API REST com CRUD completo para a tabela de pacientes, tendo como rota base `/api/paciente`;
    - No endpoint de carregar um paciente retornar também a descrição do plano vinculado ao mesmo;
- Criar um endpoint que registra um chamado novo no ElasticSearch;
- Usar o endpoint que lista pacientes para dar opção de selecionar um paciente na hora de inserir um chamado;
- Usar o endpoint que lista motivos (que estava com um bug proposital) para dar a opção de selecionar um motivo ao inserir o chamado;
- Campos de um chamado:
    - id_paciente
    - nome_paciente
    - id_motivo
    - numero_chamado
    - descricao
    - status
    - data_criacao
- Criar um endpoint que busca e lista os chamados no ElasticSearch, com controles de paginação e busca, o retorno do endpoint será usado na tarefa de frontend;

## Parte 5 - Front
- Criar uma tela para listagens dos chamados, com controle de paginação e busca, deve consumir a API criado no passo de backend;
- Adicionar um botão e funcionalidade para criar um Novo Chamado na página de chamados;
- Implementar funcionalidade para editar e visualizar os detalhes de um chamado selecionado;

## Parte 7 - Entrega
- Teste todos os requisitos para uma entrega com qualidade. Este ponto será avaliado pelo nosso time;
- Realizar o Pull Request do seu projeto javascript fullstack;
- Disponibilizar uma Collection do Postman com exemplos de uso das APIs criadas por você;
- Se desejar, descreva brevemente o critérios adotados por você no seu projeto;

## Parte 8 - opcional, porem um diferencial : segurança
- O nosso porjeto não possui qualquer critério de segurança. Fique a vontade para introduzir um sistema de segurança para acesso a API, e comunicação do Front com a API. O unico requisito é que o acesso não fique restrito a VPN ou Rede privada.
